@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Tambah Kategori</b></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('kategori.store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Nama Kategori</label>
                                <input type="text" class="form-control" name="nama_kategori" id="exampleFormControlInput1" placeholder="Ex: Sewa Kos" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Jenis Kategori</label>
                                <select name="jenis_kategori" class="form-control" id="exampleFormControlSelect1" required>
                                    @foreach(\App\Constants\JenisKategori::all() as $key => $kategori)
                                        <option value="{{ $key }}">{{ $kategori }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Deskripsi</label>
                                <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" rows="3" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Kategori</b></div>

                    <div class="card-body">
                        <a href="{{ route('kategori.create') }}" class="btn btn-primary">Tambah Kategori</a>
                        @if(!$kategories->isEmpty())
                            <table class="table table-striped" style="margin-top: 20px">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 20px">No.</th>
                                    <th>{{ __('Nama Kategori') }}</th>
                                    <th>{{ __('Jenis') }}</th>
                                    <th>{{ __('Deskripsi') }}</th>
                                    <th class="text-center pull-right">{{ __('Aksi') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($kategories as $i => $kategori)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>{{ $kategori->nama_kategori }}</td>
                                        <td>{{ \App\Constants\JenisKategori::getTitle($kategori->jenis_kategori) }}</td>
                                        <td>{{ $kategori->deskripsi }}</td>
                                        <td class="text-center pull-right" >
                                            <div class="btn-group">
                                                <a href="{{ route('kategori.edit', $kategori) }}" class="btn btn-warning">
                                                    Ubah
                                                </a>
                                                <a href="{{ route('kategori.delete', $kategori) }}" class="btn btn-danger"
                                                   onclick = "return confirm('Are you sure ?')">
                                                    Hapus
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <center>{{ $kategories->links() }}</center>
                        @else
                            <div class="px-3 text-center" style="padding: 10px">
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    No Data
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

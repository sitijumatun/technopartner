@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Tambah Transaksi</b></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('transaksi.store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Tanggal Transaksi</label>
                                <input type="date" class="form-control" name="tanggal_transaksi" id="exampleFormControlInput1" placeholder="Ex: Sewa Kos" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Jenis Transaksi</label>
                                <select name="jenis_transaksi" class="form-control" id="exampleFormControlSelect1" required>
                                    @foreach(\App\Constants\JenisKategori::all() as $key => $kategori)
                                        <option value="{{ $key }}">{{ $kategori }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Kategori</label>
                                <select name="kategori_id" class="form-control" id="exampleFormControlSelect1" required>
                                    @foreach($kategoris as $key => $kategori)
                                        <option value="{{ $kategori->id }}">{{ $kategori->nama_kategori .'-'. \App\Constants\JenisKategori::getTitle($kategori->jenis_kategori) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Nominal</label>
                                <input type="number" min="0" class="form-control" name="nominal" id="exampleFormControlInput1" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Deskripsi</label>
                                <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" rows="3" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Saldo Saat Ini</b></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ 'IDR'.number_format($saldo, 0, ',', '.')}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 20px">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Transaksi</b></div>

                    <div class="card-body">
                        <form id="frmFilter" class="row mb-3 clearfix">
                            <div class="col-sm-4">
                                <input  type="date"
                                        class="form-control" name="start_date" id="from_date" placeholder="From Date"
                                        value="{{ request('start_date') ?: date('Y-m-d') }}">
                            </div>

                            <div class="col-sm-4">
                                <input  type="date"
                                        class="form-control" name="end_date" id="to_date" placeholder="To Date"
                                        value="{{ request('end_date') ?: date('Y-m-d') }}">
                            </div>

                            <div class="col-sm-4">
                                <div class="float-right">
                                    <button class="btn btn-primary"><i class="fa fa-filter"></i> Filter</button>
                                </div>
                            </div>
                        </form>

                        <a href="{{ route('transaksi.create') }}" class="btn btn-primary">Tambah Transaksi</a>
                        @if(!$transaksis->isEmpty())
                            <table class="table table-striped" style="margin-top: 20px">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 20px">No.</th>
                                    <th>{{ __('Tanggal') }}</th>
                                    <th>{{ __('Jenis') }}</th>
                                    <th>{{ __('Kategori') }}</th>
                                    <th>{{ __('Nominal') }}</th>
                                    <th>{{ __('Deskripsi') }}</th>
                                    <th class="text-center pull-right">{{ __('Aksi') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transaksis as $i => $transaksi)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>{{ $transaksi->tanggal_transaksi }}</td>
                                        <td>{{ \App\Constants\JenisKategori::getTitle($transaksi->jenis_transaksi) }}</td>
                                        <td>{{ $transaksi->kategori->nama_kategori }}</td>
                                        <td>{{ 'IDR'.number_format($transaksi->nominal, 0, ',', '.')}}</td>
                                        <td>{{ $transaksi->deskripsi }}</td>
                                        <td class="text-center pull-right" >
                                            <div class="btn-group">
                                                <a href="{{ route('transaksi.edit', $transaksi) }}" class="btn btn-warning">
                                                    Ubah
                                                </a>
                                                <a href="{{ route('transaksi.delete', $transaksi) }}" class="btn btn-danger"
                                                   onclick = "return confirm('Are you sure ?')">
                                                    Hapus
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <center>{{ $transaksis->links() }}</center>
                        @else
                            <div class="px-3 text-center" style="padding: 10px">
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    No Data
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth','namespace' => 'Web'], static function() {
    Route::get('home', 'HomeController@index')->name('home');
    Route::resource('kategori', 'KategoriController');
    Route::get('kategori/delete/{kategori}','KategoriController@delete')->name('kategori.delete');

    Route::resource('transaksi', 'TransaksiController');
    Route::get('transaksi/delete/{transaksi}','TransaksiController@delete')->name('transaksi.delete');
});



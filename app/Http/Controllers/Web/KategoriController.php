<?php

namespace App\Http\Controllers\Web;

use App\Kategori;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|\Illuminate\View\View
     */
    public function index()
    {
        $kategories = Kategori::orderBy('created_at')->paginate(5);

        return view('kategori.index', compact('kategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'nama_kategori' => 'required',
           'jenis_kategori' => 'required',
           'deskripsi' => 'required'
        ]);

        $kategori = new Kategori();
        $kategori->fill($request->all());
        $kategori->user()->associate($request->user());
        $kategori->save()
            ? flash('Data kategori berhasil disimpan!')->success()
            : flash('Data kategori gagal disimpan!')->error();

        return redirect()->route('kategori.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Kategori $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        return view('kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Kategori $kategori
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Kategori $kategori)
    {
        $this->validate($request, [
            'nama_kategori' => 'required',
            'jenis_kategori' => 'required',
            'deskripsi' => 'required'
        ]);

        $kategori->fill($request->all());
        $kategori->user()->associate($request->user());
        $kategori->update()
            ? flash('Data kategori berhasil diubah!')->success()
            : flash('Data kategori gagal diubah!')->error();

        return redirect()->route('kategori.index');
    }


    /**
     * @param Kategori $kategori
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Kategori $kategori){
        if(count($kategori->transaksis) > 0){
            flash('Kategori memiliki data transaksi, data tidak bisa dihapus! ')->warning();
            return redirect()->route('kategori.index');
        }

        $kategori->delete()
            ? flash('Kategori Berhasil dihapus!')->success()
            : flash('Kategori Gagal dihapus!')->error();

        return redirect()->route('kategori.index');
    }


}

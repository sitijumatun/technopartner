<?php

namespace App\Http\Controllers\Web;

use App\Services\AccountingService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index(){
        $accountingService = new AccountingService();
        $saldo = $accountingService->getCurrentSaldo();
        $pemasukan = $accountingService->getCurrentPemasukan();
        $pengeluaran = $accountingService->getCurrentPengeluaran();
        return view('home', compact('saldo','pemasukan', 'pengeluaran'));
    }
}

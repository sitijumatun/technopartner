<?php

namespace App\Http\Controllers\Web;

use App\Constants\JenisKategori;
use App\Kategori;
use App\Services\AccountingService;
use App\Transaksi;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Zend\Diactoros\Response\TextResponse;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $accountingService = new AccountingService();
        $saldo = $accountingService->getCurrentSaldo();

        $today = date("Y-m-d");
        $startDateOfThisMonth = date('Y-m-01', strtotime($today));
        $endDateOfThisMonth = date('Y-m-t', strtotime($today));

        $startDate = $request->has('start_date')
            ? $request->get('start_date')
            :  $startDateOfThisMonth;

        $endDate = $request->has('end_date')
            ?  $request->get('end_date')
            : $endDateOfThisMonth;

        $transaksis = Transaksi::whereBetween('tanggal_transaksi',[Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
                    ->paginate(5)
                    ->appends($request->all());

        return view('transaksi.index', compact('transaksis', 'saldo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategoris = Kategori::all();
        return view('transaksi.create', compact('kategoris'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'tanggal_transaksi' => 'required',
           'jenis_transaksi' => 'required',
           'kategori_id' => 'required',
           'nominal' => 'required',
           'deskripsi' => 'required'
        ]);

        $kategori = Kategori::find($request->get('kategori_id'));
        if($kategori->jenis_kategori !== $request->get('jenis_transaksi')){
             flash('Kategori yang dimasukkan tidak sesuai dengan jenis transaksi!')->warning();
             return redirect()->back();
        }

        $transaksi = new Transaksi();
        $transaksi->fill($request->all());
        $transaksi->user()->associate($request->user());
        $transaksi->kategori()->associate($kategori);
        $transaksi->save()
            ? flash('Transaksi berhasil disimpan!')->success()
            : flash('Transaksi gagal disimpan!')->error();

        return redirect()->route('transaksi.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Transaksi $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi $transaksi)
    {
        $kategoris = Kategori::all();
        return view('transaksi.edit', compact('kategoris','transaksi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Transaksi $transaksi
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Transaksi $transaksi)
    {
        $this->validate($request,[
            'tanggal_transaksi' => 'required',
            'jenis_transaksi' => 'required',
            'kategori_id' => 'required',
            'nominal' => 'required',
            'deskripsi' => 'required'
        ]);

        $kategori = Kategori::find($request->get('kategori_id'));
        if($kategori->jenis_kategori !== $request->get('jenis_transaksi')){
            flash('Kategori yang dimasukkan tidak sesuai dengan jenis transaksi!')->warning();
            return redirect()->back();
        }

        $transaksi->fill($request->all());
        $transaksi->user()->associate($request->user());
        $transaksi->kategori()->associate($kategori);
        $transaksi->update()
            ? flash('Transaksi berhasil diubah!')->success()
            : flash('Transaksi gagal diubah!')->error();

        return redirect()->route('transaksi.index');
    }

    /**
     * @param Transaksi $transaksi
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Transaksi $transaksi){
        $transaksi->delete()
            ? flash('Transaksi berhasil dihapus!')->success()
            : flash('Transaksi gagal dihapus')->error();

        return redirect()->route('transaksi.index');
    }
}

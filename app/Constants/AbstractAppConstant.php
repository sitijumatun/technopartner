<?php

namespace App\Constants;

use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionException;

class AbstractAppConstant implements AppConstantInterface
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public static function all(): array
    {
        // TODO: Implement all() method.
        $constants = collect((new ReflectionClass(static::class))->getConstants());
        $values = collect();
        $constants->each(
            static function ($constant, $index) use ($values) {
                $values->put($constant, Str::title(str_replace('_', ' ', $index)));
            }
        );

        return $values->toArray();
    }

    /**
     * @param string $name
     * @return string
     * @throws ReflectionException
     */
    public static function getTitle(string $name): string
    {
        // TODO: Implement getTitle() method.
        $constants = static::all();

        return $constants[$name];
    }
}

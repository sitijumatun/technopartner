<?php
/**
 * Created by PhpStorm.
 * User: jeje
 * Date: 06/04/20
 * Time: 13:56
 */

namespace App\Constants;


class JenisKategori extends AbstractAppConstant
{
    public const PEMASUKAN = "Pemasukan";
    public const PENGELUARAN = "Pengeluaran";
}
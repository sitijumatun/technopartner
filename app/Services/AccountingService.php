<?php
/**
 * Created by PhpStorm.
 * User: jeje
 * Date: 06/04/20
 * Time: 16:10
 */

namespace App\Services;
use App\Transaksi;
use App\Constants\JenisKategori;

class AccountingService
{
    /**
     * @return int
     */
    public function getCurrentSaldo(){
        $pemasukan = $this->getCurrentPemasukan();
        $pengeluaran = $this->getCurrentPengeluaran();
        $saldo = (int) $pemasukan - (int) $pengeluaran;
        return $saldo;
    }

    /**
     * @return int
     */
    public function getCurrentPemasukan(){
        $pemasukan = Transaksi::where('jenis_transaksi', JenisKategori::PEMASUKAN)
            ->sum('nominal') ?:0 ;
        return $pemasukan;
    }

    /**
     * @return int
     */
    public function getCurrentPengeluaran(){
        $pengeluaran = Transaksi::where('jenis_transaksi', JenisKategori::PENGELUARAN)
            ->sum('nominal') ?: 0;
        return $pengeluaran;
    }
}